<?php
try {
    include_once('conexao.php');

    $med_id = filter_input(INPUT_GET, 'arq_id', FILTER_DEFAULT);

    $sql = $pdo->prepare('DELETE FROM arquivo WHERE arq_id=:arq_id');
    $sql->bindValue(':arq_id', $med_id);
    $sql->execute();

    header('Location: ../');
} catch (PDOException $e) {
    echo 'Um erro ocorreu! Erro: ' . $e->getMessage();
}
