<?php
try {
    include_once('conexao.php');
    date_default_timezone_set('Brazil/east');

    $ext = strtolower(substr($_FILES['arq_arquivo']['name'], -4));
    $name = strtolower($_POST['arq_nome']);
    $new_name = $name . date('YmdHis') . $ext;

    move_uploaded_file($_FILES['arq_arquivo']['tmp_name'], 'images/' . $new_name);

    $dados = [
        'arq_nome' => $name,
        'arq_arquivo' => $new_name
    ];

    $fields = implode(', ', array_keys($dados));
    $places = ':' . implode(', :', array_keys($dados));
    $create = "INSERT INTO arquivo ($fields) VALUES ($places)";

    $sth = $pdo->prepare($create);
    $sth->execute($dados);

    header('Location: ../?sent=true');
} catch (Exception $ex) {
    header('Location: ../sent=false');
}
