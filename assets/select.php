<?php
try {
    include_once('assets/conexao.php');

    $sql = $pdo->prepare("SELECT * FROM arquivo");
    $sql->execute();

    if ($sql->rowCount() > 0) :
        foreach ($sql as $key) : extract($key) ?>
            <div class="col s12 m6" style="position:relative">
                <img title="<?= $arq_nome ?>" class="responsive-img materialboxed" src="assets/images/<?= $arq_arquivo ?>" alt="<?= $arq_nome ?>">
                <a href="assets/delete.php?arq_id=<?= $arq_id ?>"><i title="Remover imagem" class="material-icons red-text" style="font-size:45px;cursor:pointer;position:absolute;bottom:0;left:50%;transform:translateX(-50%)">close</i></a>
            </div>
        <?php endforeach ?>
    <?php else : ?>
        <p style="margin-bottom:0">Não há imagens!</p>
    <?php endif ?>
<?php
} catch (PDOException $e) {
    echo 'Um erro ocorreu! Erro: ' . $e->getMessage();
}
