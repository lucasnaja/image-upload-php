<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/src/index.css">
    <link rel="stylesheet" href="assets/src/materialize.min.css">
    <title>Upload de Imagem</title>
</head>

<body class="grey lighten-3">
    <div class="container">
        <div class="card-panel left-div-margin z-depth-2">
            <h1 class="flow-text" style="margin:0 0 5px">Upload de Arquivos</h1>
            <label>Upload de Arquivos com PHP</label>
            <div class="divider"></div>

            <form action="assets/upload.php" method="post" enctype="multipart/form-data">
                <div class="row" style="margin:15px 0 0">
                    <div class="col s12 input-field">
                        <label>Nome do Arquivo</label>
                        <input placeholder="Nome" type="text" class="validate" name="arq_nome" oninvalid="this.setCustomValidity('Preencha esse campo com o nome da imagem.')" oninput="setCustomValidity('')" required>
                    </div>

                    <div style="margin-top:0" class="col s12 file-field input-field">
                        <div class="btn indigo darken-4">
                            <span><i class="material-icons left">cloud_upload</i>Selecionar</span>
                            <input type="file" name="arq_arquivo" oninvalid="this.setCustomValidity('Seleciona a imagem para continuar.')" oninput="setCustomValidity('')" required>
                        </div>

                        <div class="file-path-wrapper">
                            <input placeholder="Selecionar imagem" type="text" class="file-path validate">
                        </div>
                    </div>

                    <div class="divider"></div>

                    <div class="col s12">
                        <input title="Enviar imagem" class="btn indigo darken-4 right" type="submit" value="Enviar">
                    </div>

                    <div class="left-div indigo darken-4"></div>
                </div>
            </form>
        </div>

        <div class="card-panel left-div-margin z-depth-2">
            <h1 class="flow-text" style="margin:0 0 5px">Imagens</h1>
            <label>Lista de imagens</label>
            <div class="divider" style="margin-bottom:10px"></div>

            <div class="row" style="margin-bottom:0">
                <?php include_once('assets/select.php') ?>
            </div>

            <div class="left-div indigo darken-4"></div>
        </div>
    </div>

    <script src="assets/src/materialize.min.js"></script>
    <script>
        M.Materialbox.init(document.querySelectorAll('.materialboxed'))
    </script>
    <?php
    if (isset($_GET['sent'])) {
        $sent = $_GET['sent'];

        if ($sent === 'true') : ?>
            <script>
                M.toast({
                    html: 'Imagem enviada com sucesso.',
                    classes: 'green'
                })
            </script>
        <?php elseif ($sent === 'false') : ?>
            <script>
                M.toast({
                    html: 'Falha ao enviar imagem.',
                    classes: 'red accent-4'
                })
            </script>
        <?php endif ?>
    <?php } ?>
</body>

</html>